import sbt._
import play.Play.autoImport._

object Version {
  val dbunit = "2.4.7"
  val postgresql = "9.1-901.jdbc4"
  val mockito = "1.9.5"
  val scalatest = "2.2.4"
}

object Library {
  val dbunit = "org.dbunit" % "dbunit" % Version.dbunit
  val postgresql = "postgresql" % "postgresql" % Version.postgresql
  val mockito = "org.mockito" % "mockito-all" % Version.mockito
  val scalatest = "org.scalatest" %% "scalatest" % Version.scalatest
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.1"

  val runDep = run(
    jdbc,
    anorm,
    cache,
    ws,
    postgresql
  )

  val testDep = test(
    mockito,
    scalatest,
    dbunit
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
