import sbt._
import sbt.Keys._
import com.typesafe.sbt.packager.Keys._
import com.typesafe.sbt.SbtNativePackager._

object ApplicationBuild extends Build {

  val appName = "routing-server"
  val appVersion = "1.3"
  val appOrganisation = "xivo"

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in(Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(
      DockerSettings.settings: _*
    )

  object DockerSettings {
    val settings = Seq(
      maintainer in Docker := "Jirka HLAVACEK <jhlavacek@avencall.com>",
      dockerBaseImage := "java:openjdk-8u66-jdk",
      dockerExposedPorts := Seq(9001),
      dockerExposedVolumes := Seq("/conf"),
      dockerRepository := Some("xivoxc"),
      dockerEntrypoint := Seq("bin/routing_server_docker")
    )
  }
}
