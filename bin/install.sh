#!/bin/bash
set -e

function check_is_root() {
    if [ "$(id -u)" -ne "0" ]; then
        echo "Ce script doit être exécuté en root"
        exit -1
    fi;
}

cd /tmp
check_is_root
docker pull xivoxc/routing-server
mkdir -p /etc/docker/routing-server
wget https://gitlab.com/xivo-utils/routing-server/raw/master/conf/application.conf
wget https://gitlab.com/xivo-utils/routing-server/raw/master/conf/logback.xml
mv application.conf logback.xml /etc/docker/routing-server
mkdir -p /var/log/routing-server
chown daemon:daemon /var/log/routing-server
echo "Installation terminée. Pour lancer l'application, exécutez la cmmande suivante :
docker run -t -d --name=routing-server --net host -v /etc/docker/routing-server:/conf/ -v /var/log/routing-server:/var/log/routing-server xivoxc/routing-server -Dconfig.file=/conf/application.conf -Dlogger.file=/conf/logback.xml -Dhttp.port=9001"
