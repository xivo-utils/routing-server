package model

import models.Route
import play.api.libs.json.Json
import play.api.test.{FakeApplication, WithApplication, PlaySpecification}
import anorm._

class RouteSpec extends PlaySpecification {

  implicit val connection = DBUtil.getConnection()

  val testConfig = Map(
    "db.default.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.default.user" -> "asterisk",
    "db.default.password" -> "asterisk",
    "db.default.driver" -> "org.postgresql.Driver"
  )


  "The Route singleton" should {
    "find one of the routes associated to the given digits" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      DBUtil.setupDB("database.xml")
      val r1 = Route(Some(1), "1200", false, "(.*)", "\\1", None, "to-xivo1")
      val r2 = Route(Some(2), "12", true, "(.*)", "\\1", Some("test"), "to-xivo2")
      val r3 = Route(Some(3), "13", true, "(.*)", "\\1", None, "to-xivo1")
      List(r1, r3).foreach(insertRoute)

      Route.read("1200") shouldEqual Some(r1)

      DBUtil.cleanTable("route")
      List(r2, r3).foreach(insertRoute)

      Route.read("1200") shouldEqual Some(r2)
      Route.read("1400") shouldEqual None
    }

    "serialize a Route to Json" in {
      Json.toJson(Route(Some(3), "1000", true, "(.*)", "\\1", Some("my-subroutine"), "to-xivo1")) shouldEqual Json.obj(
      "id" -> 3,
      "digits" -> "1000",
      "prefix" -> true,
      "regexp" -> "(.*)",
      "target" -> "\\1",
      "subroutine" -> "my-subroutine",
      "context" -> "to-xivo1")
    }
  }

  def insertRoute(r: Route) =
    SQL("INSERT INTO route(id, digits, prefix, regexp, target, subroutine, context) VALUES " +
      "({id}, {digits}, {prefix}, {regexp}, {target}, {subroutine}, {context})").on(
    'id -> r.id, 'digits -> r.digits, 'prefix -> r.prefix, 'regexp -> r.regexp, 'target -> r.target, 'subroutine -> r.subroutine,
        'context -> r.context).executeUpdate()
}
