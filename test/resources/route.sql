DROP TABLE IF EXISTS route CASCADE;

CREATE TABLE route (
    id SERIAL PRIMARY KEY,
    length integer,
    digits character varying(50) NOT NULL,
    prefix boolean DEFAULT false NOT NULL,
    regexp character varying(50) DEFAULT '(.*)$'::character varying NOT NULL,
    target character varying(50) DEFAULT '\1'::character varying NOT NULL,
    subroutine character varying(50),
    context character varying(50) DEFAULT 'to_pit_stop'::character varying NOT NULL,
    comment character varying(1000),
    UNIQUE(digits, prefix)
);

CREATE OR REPLACE FUNCTION route_insert_prefix_length() RETURNS TRIGGER AS $route_calculate_length$
BEGIN
    NEW.length = length(NEW.digits);
    RETURN NEW;
END;
$route_calculate_length$ LANGUAGE plpgsql;

CREATE TRIGGER route_insert_prefix_length BEFORE INSERT OR UPDATE ON route FOR EACH ROW EXECUTE PROCEDURE route_insert_prefix_length();
