package controllers

import models.{Route, RouteInterface}
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import play.api.test.{FakeApplication, FakeRequest, PlaySpecification, WithApplication}

class ApplicationSpec extends PlaySpecification with MockitoSugar {

  val testConfig = Map(
    "db.default.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.default.user" -> "asterisk",
    "db.default.password" -> "asterisk",
    "db.default.driver" -> "org.postgresql.Driver"
  )

  "The Application controller" should {
    "return the route formatted to JSON" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Application.routes = mock[RouteInterface]
      val myRoute = Route(Some(2), "2000", true, "(.*)", "\\1", Some("my_subroutine"), "to-xivo1")
      stub(Application.routes.read("2000")).toReturn(Some(myRoute))

      val res = route(FakeRequest(GET, "/route?digits=2000")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(myRoute)
    }

    "return 404 if the route does not exist" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Application.routes = mock[RouteInterface]
      stub(Application.routes.read("2000")).toReturn(None)

      val res = route(FakeRequest(GET, "/route?digits=2000")).get

      status(res) shouldEqual NOT_FOUND
    }
  }

}
