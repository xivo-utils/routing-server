package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import models.{RouteInterface, Route}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._
import java.util.concurrent.TimeUnit.SECONDS
import play.api.Play.current

object Application extends Controller {

  val Timeout: Int = Play.application.configuration.getInt("requestTimeout").get
  var routes: RouteInterface = Route

  def getRoute(digits: String) = Action.async {
    Logger("Application").debug("getting routes for digits: " + digits)
    val futureRoute: Future[Option[Route]] = scala.concurrent.Future { routes.read(digits) }
    val timeoutRoute = play.api.libs.concurrent.Promise.timeout("Timeout", Timeout, SECONDS)
    Future.firstCompletedOf(Seq(futureRoute, timeoutRoute)).map {
      case Some(route) => Ok(Json.toJson(route.asInstanceOf[Route]))
      case None =>Logger("Application").info("Get routes for digits: " + digits + " returned no result")
        NotFound(Json.toJson(Map("Result" -> "No such element")))
      case ko: String =>
        Logger("Application").error("Get routes for digits: " + digits + " failed with timeout")
        RequestTimeout(Json.toJson(Map("Result" -> "Request processing timeout")))
    } recover {
      case e: Exception =>
        Logger("Application").error("Get routes for digits: " + digits + " failed with exception: " + e.toString)
        InternalServerError(Json.toJson(Map("Result" -> "Request processing failed")))
    }
  }
}