package models

import play.api.Play.current
import play.api.db.DB
import anorm._
import anorm.SqlParser._
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._


case class Route (
  id: Option[Long],
  digits: String,
  prefix: Boolean,
  regexp: String,
  target: String,
  subroutine: Option[String],
  context: String
)

trait RouteInterface {
  def read(digits: String): Option[Route]
}


object Route extends RouteInterface {

  implicit val writes = ((__ \ "id").write[Option[Long]] and
    (__ \ "digits").write[String] and
    (__ \ "prefix").write[Boolean] and
    (__ \ "regexp").write[String] and
    (__ \ "target").write[String] and
    (__ \ "subroutine").write[Option[String]] and
    (__ \ "context").write[String])(unlift(Route.unapply))

  val simpleRoute = get[Option[Long]]("id") ~
    get[String]("digits") ~
    get[Boolean]("prefix") ~
    get[String]("regexp") ~
    get[String]("target") ~
    get[Option[String]]("subroutine") ~
    get[String]("context") map {
      case id ~ digits ~ prefix ~ regexp ~ target ~ subroutine ~ context => Route(id, digits, prefix, regexp, target, subroutine, context)
    }


  override def read(digits: String): Option[Route] = {
    DB.withConnection { implicit c =>
      val result = SQL(
        """SELECT id, digits, prefix, regexp, target, subroutine, context FROM route WHERE (digits = {digits} AND NOT prefix)
          OR (prefix AND {digits} LIKE digits || '%') ORDER BY length DESC, prefix LIMIT 1""").on('digits -> digits).as(simpleRoute *).headOption
      Logger("Route").debug("Query result for digit: " + digits + " : " + result )
      println("Query result for digit: " + digits + " : " + result)
      result
    }
  }
}
